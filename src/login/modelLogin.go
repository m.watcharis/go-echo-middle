package login

import "github.com/labstack/echo"

type UserRequestLogin struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type UserResponseLogin struct {
	Email    string      `json:"email"`
	Password string      `json:"password"`
	Token    interface{} `json:"token"`
}

type ValidateUser struct {
	Email    string
	Password string
}

type ResponseJson struct {
	Message string      `json:"message"`
	Status  string      `json:"status"`
	Data    interface{} `json:"data"`
}

type ServiceLogin interface {
	// LoginService(data UserRequestLogin) (res ResponseJson, err error)
	Login(c echo.Context) error
	Register(c echo.Context) error
}
