package login

import (
	"errors"
	"fmt"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
)

type service struct {
	loginserv ServiceLogin
}

func NewLoginService() ServiceLogin {
	return &service{}
}

func LoginService(data UserRequestLogin) (res ResponseJson, err error) {

	var validteUser ValidateUser

	validteUser = ValidateUser{"own@email.com", "password123"}

	if data.Email != validteUser.Email {
		return ResponseJson{Message: "invalid user login", Status: "fail", Data: ""}, errors.New("")
	} else {

		// Create token
		token := jwt.New(jwt.SigningMethodHS256)
		fmt.Println("token :", token)

		// Set claims
		claims := token.Claims.(jwt.MapClaims)
		fmt.Println("claims :", claims)

		claims["email"] = data.Email
		claims["exp"] = time.Now().Add(time.Hour * 72).Unix()

		fmt.Println("claims :", claims)

		// Generate encoded token and send it as response.
		t, err := token.SignedString([]byte("secret"))
		if err != nil {
			return ResponseJson{Message: "Generate encoded token fail", Status: "fail", Data: ""}, err
		}

		fmt.Println("t :", t)

		return ResponseJson{
			Message: "login success",
			Status:  "success",
			Data:    UserResponseLogin{Email: data.Email, Password: data.Password, Token: t}}, nil
	}
}

func (s *service) Login(c echo.Context) error {

	// reqbody := &UserRequestLogin{}
	reqbody := new(UserRequestLogin)

	checkUser := &ValidateUser{"own@email.com", "password123"}
	fmt.Println("checkUser :", checkUser)

	fmt.Println("reqbody :", reqbody)

	if err := c.Bind(reqbody); err != nil {
		return err
	}

	fmt.Println("reqbody-c :", reqbody.Email)

	var data UserRequestLogin

	data = UserRequestLogin{
		reqbody.Email,
		reqbody.Password,
	}

	loginservice, err := LoginService(data)

	if err != nil && loginservice.Status != "success" {
		// fmt.Println("loginservice :", loginservice)
		fmt.Println("err :", err)
		return c.JSON(http.StatusNotFound, loginservice)
	} else {

		fmt.Println("loginservice :", loginservice)
		return c.JSON(http.StatusOK, loginservice)
	}
}

func (s *service) Register(c echo.Context) error {

	return c.JSON(http.StatusOK, ResponseJson{Message: "register success", Status: "success", Data: ""})
}
