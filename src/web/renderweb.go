package web

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo"
)

type service struct{}

func NewTemplateService() ServiceTemplate {
	return &service{}
}

func (s *service) RenderTemplate(c echo.Context) error {
	fmt.Println("in RenderTemplate")
	return c.Render(http.StatusOK, "index.html", echo.Map{"title": "Index title!"})
}
