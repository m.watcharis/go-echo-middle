package web

import (
	"html/template"

	"github.com/labstack/echo"
)

type TemplateRenderer struct {
	templates *template.Template
}

type ServiceTemplate interface {
	RenderTemplate(c echo.Context) error
}

var Renderers *TemplateRenderer
