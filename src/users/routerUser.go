package users

import (
	validate "go-testmiddleware/src/middleware"

	"github.com/labstack/echo"
)

type httpRouter struct {
	httproute ServiceUser
}

func NewHttpRouterUser(service ServiceUser) httpRouter {
	return httpRouter{
		httproute: service,
	}
}

func (h httpRouter) RouterPublics(e *echo.Group) {
	// e.Use(validate.ValidateUser)
	router := e.Group("/api/v1/user")
	router.POST("/insertuser", h.httproute.InsertUser)
	router.GET("/getuser", h.httproute.GetUserById, validate.ValidateUser)
	router.PUT("/updateuser", h.httproute.UpdateUser)
	router.GET("/testreq", h.httproute.TestSendRequest)
}
