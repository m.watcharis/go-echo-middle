package users

import (
	"github.com/labstack/echo"
)

// type UserParam struct {
// 	UserId string `param:"id" query:"id" form:"id" json:"id" xml:"id"`
// }

type UserDetail struct {
	UserId    int    `json:"user_id,omitemty"`
	Firstname string `json:"firstname"`
	Email     string `json:"email"`
	Phone     string `json:"phone"`
}

type ServiceUser interface {
	GetUserById(c echo.Context) error
	InsertUser(c echo.Context) error
	UpdateUser(c echo.Context) error
	TestSendRequest(c echo.Context) error
}

type DataTestRequestMethodsPost struct {
	Otp  string `json:"otp"`
	Port string `json:"port"`
	Host string `json:"host"`
}
