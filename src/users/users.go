package users

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"reflect"
	"strconv"
	"time"

	"github.com/labstack/echo"

	modelresponse "go-testmiddleware/src/login"
)

type service struct {
	userserv ServiceUser
}

func NewUserService() ServiceUser {
	return &service{}
}

var storeUser []UserDetail

func resultStringFormat(a interface{}, b string) (resultString string, err error) {
	fmt.Println("in resultStringFormat")

	// ประกาศตัวแปรชนิด interface example -->> var test interface{} = 1
	// Check type interface by switvh case
	switch a.(type) {
	case string:
		if b != "" {

			if c, err := strconv.Atoi(b); err == nil {

				x := strconv.Itoa(c)

				resultString = fmt.Sprintf("USERID : type = %T / value = %v", c, x)
				// fmt.Println("resultString :", resultString)
			}
		}
		return resultString, nil
	default:
		resultString = "unknow"
		// fmt.Println("resultString :", resultString)
		return resultString, errors.New("Not Found Type")
	}
}

func (s *service) GetUserById(c echo.Context) error {

	// user Param /getuser/:id
	// user QueryParam /getuser?id=?

	// id := c.Param("id")
	userDetail := &UserDetail{
		UserId:    1,
		Firstname: "Olando",
		Email:     "Olando@mail.com",
		Phone:     "0899995647",
	}

	user_id := c.QueryParam("user_id")

	user_id_type := reflect.TypeOf(user_id)
	fmt.Println("user_id_type :", user_id_type)

	user_id_value := reflect.ValueOf(user_id)
	fmt.Println("user_id_value :", user_id_value)

	// convert type string to int Use strconv.Atoi (ascii to int) / int to string Use strconv.Itoa (int to ascii)
	userIdToInt, err := strconv.Atoi(user_id)
	// fmt.Printf("type of  userIdToInt: %T", userIdToInt)
	if err == nil {
		fmt.Printf("userIdToInt = type: %T , value: %v\n", userIdToInt, userIdToInt)
	}

	returnStringFormat, err := resultStringFormat(user_id, user_id)
	if err != nil {
		fmt.Println("error returnStringFormat :", err)
	}
	fmt.Println("returnStringFormat :", returnStringFormat)

	t1 := time.Now()
	t2 := t1.Add(time.Millisecond * 341)
	// t2 := time.Now()

	fmt.Println(t1)
	fmt.Println(t2)

	diff := t2.Sub(t1)
	defer fmt.Println(diff)

	if userDetail.UserId == userIdToInt {
		// fmt.Println("userDetail.UserId :", userDetail.UserId)
		return c.JSON(http.StatusOK, modelresponse.ResponseJson{Message: "get user by id success", Status: "success", Data: userDetail})
	} else {
		return c.JSON(http.StatusOK, modelresponse.ResponseJson{Message: "not found user", Status: "fail", Data: ""})
	}
}

func Find(stroeuser []UserDetail, userid int) bool {
	for _, value := range stroeuser {
		fmt.Println("value :", value)
		if value.UserId == userid {
			return true
		}
	}
	return false
}

func (s *service) InsertUser(c echo.Context) error {

	reqbody := new(UserDetail)

	if err := c.Bind(reqbody); err != nil {
		return err
	}

	user_detail := &UserDetail{
		UserId:    reqbody.UserId,
		Firstname: reqbody.Firstname,
		Email:     reqbody.Email,
		Phone:     reqbody.Phone,
	}

	fmt.Println("user_detail :", user_detail.UserId)

	// convert struct to json by json.Marshal
	testJsonMashal, err := json.Marshal(user_detail)
	if err != nil {
		fmt.Println("err testJsonMashal :", err)
	}
	fmt.Println("testJsonMashal :", string(testJsonMashal))

	if len(storeUser) == 0 {
		// fmt.Println("if")
		storeUser = append(storeUser, *user_detail)
		fmt.Println("storeUser :", reflect.ValueOf(storeUser).Index(0).Interface())
	} else {
		fmt.Println("else")

		checkDataStore := Find(storeUser, reqbody.UserId)
		fmt.Printf("checkDataStore : %t\n", checkDataStore)

		if resultCheckData := "Duplicate data"; checkDataStore == true {
			return c.JSON(http.StatusOK, modelresponse.ResponseJson{Message: resultCheckData, Status: "fail", Data: ""})
		} else {
			storeUser = append(storeUser, *user_detail)
		}
	}
	return c.JSON(http.StatusOK, modelresponse.ResponseJson{Message: "insert success", Status: "success", Data: ""})
}

func (s *service) UpdateUser(c echo.Context) error {
	// fmt.Println("storeUser :", storeUser)
	reqbody := &UserDetail{}

	if err := c.Bind(reqbody); err != nil {
		return err
	}
	// fmt.Println("reqbody :", reqbody)
	if len(storeUser) != 0 {

		checkBeforeUpdate := Find(storeUser, reqbody.UserId)

		if checkBeforeUpdate == true {

			for i := 0; i < len(storeUser); i++ {
				// fmt.Println(storeUser[i])
				if storeUser[i].UserId != reqbody.UserId {
					continue
				} else {
					storeUser[i].Firstname = reqbody.Firstname
					storeUser[i].Email = reqbody.Email
					storeUser[i].Phone = reqbody.Phone
					// fmt.Println("storeUser :", storeUser)
				}
			}

			return c.JSON(http.StatusOK, modelresponse.ResponseJson{Message: "update success", Status: "success", Data: storeUser})
		} else {
			return c.JSON(http.StatusOK, modelresponse.ResponseJson{Message: "update fail", Status: "fail", Data: "Not found User in storeUser"})
		}
	} else {
		// fmt.Println("No data")
		return c.JSON(http.StatusOK, modelresponse.ResponseJson{Message: "update fail", Status: "fail", Data: "No data"})
	}
}

func (s *service) TestSendRequest(c echo.Context) error {

	//###################### request methods GET ######################

	urlGetMethods := "http://127.0.0.1:4567"
	resp, err := http.Get(urlGetMethods)

	if err != nil {
		log.Fatal("err :", err)
	}
	fmt.Println("resp :", resp.Header)

	defer resp.Body.Close()

	if resp.Header.Get("Content-Type") == "application/json" {

		body, err := ioutil.ReadAll(resp.Body)

		if err != nil {
			log.Fatal("err :", err)
		}
		fmt.Println("body :", string(body))
	}

	//################## request methods POST #########################

	payloadTestGoMethodsPost := &DataTestRequestMethodsPost{
		Otp:  "456789",
		Port: "1323",
		Host: "127.0.0.1",
	}

	payloadBody, err := json.Marshal(payloadTestGoMethodsPost)

	if err != nil {
		log.Fatal(err)
	}

	urlPostMethods := "http://127.0.0.1:4567/gotestpost"

	resPost, errPostMethods := http.Post(urlPostMethods, "application/json", bytes.NewBuffer(payloadBody))
	resPost.Header.Set("Content-Type", "application/json")

	if errPostMethods != nil {
		log.Fatal(errPostMethods)
	}

	defer resPost.Body.Close()

	bodyPost, errPost := ioutil.ReadAll(resPost.Body)

	if errPost != nil {
		log.Fatal(errPost)
	}

	fmt.Println("bodyPost :", bodyPost)
	fmt.Println("bodyPost :", string(bodyPost))
	// fmt.Println(reflect.TypeOf(bodyPost))

	var res modelresponse.ResponseJson

	// json.Unmarshal คือ การแปลง byte array ให้อยู่ในรูปแบบ type struct ใน golang
	if err := json.Unmarshal(bodyPost, &res); err != nil {
		panic(err)
	}
	fmt.Println("res :", res)

	if res.Status = "success"; err == nil {

		// กรณี เจอ map[string]interface{}
		// type res.Data เป็น map[string]interface{} ทำให้เราไม่สามารถ access เข้าไปเอาค่าของ interface ได้
		// แปลง res.Data ให้อยู่ในรูปแบบของ byte array โดย json.Marshal

		resultJson, err := json.Marshal(res.Data)

		if err != nil {
			log.Fatal(err)
		}
		fmt.Println("resultJson :", resultJson)
		fmt.Println("resultJson :", string(resultJson))

		// แปลง byte array ของ ่ map[string]interface{} ให้เป็น struct โดย json.Unmarshal(Decode)
		var result DataTestRequestMethodsPost
		if err := json.Unmarshal(resultJson, &result); err != nil {
			log.Fatal(err)
		}

		fmt.Printf("result : %v\n", result)
		return c.JSON(http.StatusOK, modelresponse.ResponseJson{Message: "test send request complete", Status: "success", Data: result})
	} else {
		return c.JSON(http.StatusOK, modelresponse.ResponseJson{Message: "req post fail", Status: "fail", Data: ""})
	}
}
