package middleware

import (
	"fmt"

	"github.com/labstack/echo"
)

var methods = []string{"GET", "POST", "PUT", "DELETE"}

func validateMethodsApi(methodsapi string) bool {
	for v := range methods {
		fmt.Println(v)
		if methods[v] == methodsapi {
			return true
		}
	}
	return false
}

func ValidateUser(next echo.HandlerFunc) echo.HandlerFunc {
	// c.Request().Header.Get("Content-Type")
	// next(c)
	return func(c echo.Context) error {
		methods := c.Request().Method
		realip := c.RealIP()
		header := c.Request().Header

		fmt.Println("methods :", methods)
		fmt.Println("realip :", realip)
		fmt.Println("header :", header)

		if validateMethodsApi(methods) == true {
		}
		// return next(c)
		return next(c)
	}
}
