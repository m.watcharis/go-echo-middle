package main

import (
	"fmt"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"

	login "go-testmiddleware/src/login"
	users "go-testmiddleware/src/users"
	web "go-testmiddleware/src/web"
)

func responseSize(url string) {
	fmt.Println("Step1: ", url)
	response, err := http.Get(url)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Step2: ", url)
	defer response.Body.Close()

	fmt.Println("Step3: ", url)
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Step4: ", len(body))
}

// TemplateRenderer is a custom html/template renderer for Echo framework
type TemplateRenderer struct {
	templates *template.Template
}

// Render renders a template document
func (t *TemplateRenderer) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	fmt.Println("t :", &t)
	// Add global methods if data is a map
	if viewContext, isMap := data.(map[string]interface{}); isMap {
		viewContext["reverse"] = c.Echo().Reverse
	}
	return t.templates.ExecuteTemplate(w, name, data)
}

func main() {
	fmt.Println("hello world")
	// go responseSize("https://www.golangprograms.com")
	// go responseSize("https://coderwall.com")
	// go responseSize("https://stackoverflow.com")
	// time.Sleep(10 * time.Second)

	e := echo.New()

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{http.MethodGet, http.MethodPut, http.MethodPost, http.MethodDelete},
	}))

	renderer := &TemplateRenderer{
		templates: template.Must(template.ParseGlob("publice/*.html")),
	}

	e.Renderer = renderer

	RouterPublic := e.Group("/publice")
	endpointlogin := login.NewLoginService()
	endpointtemplate := web.NewTemplateService()

	RouterPublic.POST("/login", endpointlogin.Login)
	RouterPublic.POST("/register", endpointlogin.Register)
	RouterPublic.GET("/", endpointtemplate.RenderTemplate)

	// group route
	endpointusers := users.NewUserService()
	endpointhttpuser := users.NewHttpRouterUser(endpointusers)
	endpointhttpuser.RouterPublics(RouterPublic)

	e.Logger.Fatal(e.Start(":1323"))
}
